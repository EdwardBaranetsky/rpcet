package user

import (
	"github.com/jmoiron/sqlx"
)

type User struct {
	Id         uint64 `db:"id"`
	Login      string `db:"login"`
	CreateTime int64  `db:"create_time"`
}

type Data struct {
	Id    uint64 `json:"id,omitempty"`
	Login string `json:"login,omitempty"`
}

type UserModel struct {
	Database *sqlx.DB
}

func (model *UserModel) Create(data *Data, id *uint64) error {
	err := model.Database.Get(&(*id), `INSERT INTO users(login) VALUES($1) RETURNING id`, data.Login)

	return err
}

func (model *UserModel) SelectById(data *Data, u *User) error {
	*u = User{
		Id: data.Id,
	}
	err := model.Database.Get(&(*u), `SELECT login, create_time FROM users WHERE id = $1`, data.Id)

	return err
}

func (model *UserModel) Update(data *Data, out *int) error {
	_, err := model.Database.Exec(`UPDATE users SET login = $1 WHERE id = $2`, data.Login, data.Id)
	*out = 1

	return err
}

func (model *UserModel) Delete(data *Data, out *int) error {
	_, err := model.Database.Exec(`DELETE FROM users WHERE id = $1`, data.Id)
	*out = 1

	return err
}
