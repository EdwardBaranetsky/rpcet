-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE IF NOT EXISTS users (
    id BIGSERIAL,
    login TEXT DEFAULT '',
    create_time BIGINT DEFAULT extract(epoch from now())
);
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE users;