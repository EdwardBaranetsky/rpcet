package integration

import (
	"bytes"
	"net/http"
	"net/http/httptest"

	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"test/RPCET/handler"
)

const (
	BaseHost = "http://127.0.0.1:7777"
	jsonrpc  = "2.0"
)

type TestContext struct {
	RealContext *handler.Context
	Router      *mux.Router
}

func (ctx *TestContext) TestPostJson(str_url string, headers map[string]string, params map[string]interface{}) (*httptest.ResponseRecorder, *http.Request) {

	params["jsonrpc"] = jsonrpc
	paramsBytes, err := json.Marshal(params)
	if err != nil {
		log.Println("[TestPostJson]cannot parse params, err:", err)
		return nil, nil
	}
	r := httptest.NewRequest("POST", BaseHost+str_url, bytes.NewBuffer(paramsBytes))
	r.Header.Set("Content-Type", "application/json")

	if headers != nil {
		for k, v := range headers {
			r.Header.Set(k, v)
		}
	}

	w := httptest.NewRecorder()
	ctx.Router.ServeHTTP(w, r)
	return w, r
}
