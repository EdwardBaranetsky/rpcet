package integration

import (
	"encoding/json"
	. "github.com/smartystreets/goconvey/convey"
	"net/http"
	"test/RPCET/model/user"
)

var userLogin = `misterX`

type rpcUserResponse struct {
	Result user.User
	Error  error
}

func (ctx *TestContext) TestCreateUser() {

	w, _ := ctx.TestPostJson(`/rpc`, nil, map[string]interface{}{
		`params`: []user.Data{
			{Login: userLogin},
		},
		`method`: `UserModel.Create`,
	})
	So(w.Code, ShouldEqual, http.StatusOK)
}

func (ctx *TestContext) TestSelectUser() {

	w, _ := ctx.TestPostJson(`/rpc`, nil, map[string]interface{}{
		`params`: []user.Data{
			{Id: 1},
		},
		`method`: `UserModel.SelectById`,
	})
	So(w.Code, ShouldEqual, http.StatusOK)

	var resp rpcUserResponse
	json.Unmarshal(w.Body.Bytes(), &resp)
	So(resp.Error, ShouldEqual, nil)

	So(resp.Result.Id, ShouldEqual, 1)
	So(resp.Result.Login, ShouldEqual, userLogin)
	So(resp.Result.CreateTime, ShouldBeGreaterThan, 0)
}

func (ctx *TestContext) TestUpdateUser() {
	userLogin = "misterB"
	w, _ := ctx.TestPostJson(`/rpc`, nil, map[string]interface{}{
		`params`: []user.Data{
			{Id: 1, Login: userLogin},
		},
		`method`: `UserModel.Update`,
	})
	So(w.Code, ShouldEqual, http.StatusOK)

	ctx.TestSelectUser()
}

func (ctx *TestContext) TestDeleteUser() {
	w, _ := ctx.TestPostJson(`/rpc`, nil, map[string]interface{}{
		`params`: []user.Data{
			{Id: 1},
		},
		`method`: `UserModel.Delete`,
	})
	So(w.Code, ShouldEqual, http.StatusOK)
}
