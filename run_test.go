// +build integration

package main

import (
	"testing"

	"github.com/pressly/goose"
	. "github.com/smartystreets/goconvey/convey"
	"io/ioutil"
	"log"
	"os"
	"test/RPCET/handler"
	"test/RPCET/integration"
)

func TestMain(t *testing.T) {

	ctx := handler.InitContext()

	tctx := &integration.TestContext{
		RealContext: ctx,
		Router:      handler.InitRoutes(ctx),
	}

	Convey("Database Context", t, func() {

		Convey("TestUserModel", func() {
			tctx.TestCreateUser()
			tctx.TestSelectUser()
			tctx.TestUpdateUser()
			tctx.TestDeleteUser()
		})

		Reset(func() {

			//shut up log, cause flood
			log.SetOutput(ioutil.Discard)
			err := goose.Reset(tctx.RealContext.Database.DB, "./model/db/migrations")
			log.SetOutput(os.Stdout)
			if err != nil {
				log.Println("[CONTEXT_INIT] Cant run down migrations (goose):", err)
				t.Fail()
			}

			log.SetOutput(ioutil.Discard)
			err = goose.Up(tctx.RealContext.Database.DB, "./model/db/migrations")
			log.SetOutput(os.Stdout)
			if err != nil {
				log.Println("[CONTEXT_INIT] Cant run up migrations (goose):", err)
				t.Fail()
			}
		})

	})
}
