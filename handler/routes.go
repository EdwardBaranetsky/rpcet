package handler

import (
	"github.com/gorilla/mux"
	"io"
	"log"
	"net/http"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type HttpConn struct {
	in  io.Reader
	out io.Writer
}

func (c *HttpConn) Read(p []byte) (n int, err error)  { return c.in.Read(p) }
func (c *HttpConn) Write(d []byte) (n int, err error) { return c.out.Write(d) }
func (c *HttpConn) Close() error                      { return nil }

type RPCHandler struct {
	rpcServer *rpc.Server
}

func (h *RPCHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	serverCodec := jsonrpc.NewServerCodec(&HttpConn{
		in:  r.Body,
		out: w,
	})
	w.Header().Set("Content-type", "application/json")
	err := h.rpcServer.ServeRequest(serverCodec)
	if err != nil {
		log.Printf("Error while serving JSON request: %v", err)
		http.Error(w, `{"error":"can't serve request"}`, http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

func InitRoutes(ctx *Context) *mux.Router {
	r := mux.NewRouter()

	rpcServer := rpc.NewServer()

	ctx.registerModels(rpcServer)

	rpcHandler := &RPCHandler{
		rpcServer: rpcServer,
	}

	r.Handle("/rpc", rpcHandler)

	return r
}
