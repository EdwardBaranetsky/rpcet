package handler

import (
	"github.com/jmoiron/sqlx"
	"log"
	"test/RPCET/model/user"
	"github.com/pressly/goose"
	"net/rpc"
	"test/RPCET/config"
	"test/RPCET/model/db"
)

type Context struct {
	Database       *sqlx.DB
	UserModel *user.UserModel
}

func InitContext() *Context {
	ctx := Context{}

	ctx.Database = db.InitPostgres(config.EnvConfig.DBConfig)
	if ctx.Database == nil {
		log.Println("[SERVER_INIT] Cant connect to database!")
		return nil
	}

	err := goose.Up(ctx.Database.DB, "./model/db/migrations")
	if err != nil {
		log.Println("[SERVER_INIT] Cant run migrations (goose):", err)
		return nil
	}

	ctx.UserModel = &user.UserModel{
		Database: ctx.Database,
	}

	return &ctx
}

func (server *Context) registerModels(rpcServer *rpc.Server) {
	rpcServer.Register(server.UserModel)
}