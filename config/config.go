package config

type AppConfig struct {
	ListenPort         uint16
	DBConfig DBConfig
}

type DBConfig struct {
	Host     string
	Port     uint16
	User     string
	Password string
	Database string
	SSL      bool
}

var EnvConfig = AppConfig{
	ListenPort:       7777,
	DBConfig: DBConfig{
		Database: "rpcet",
		Host:     "localhost",
		User:     "postgres",
		Password: "123",
		Port:     5432,
	},
}
