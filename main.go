package main

import (
	"net/http"
	"log"
	"strconv"
	"time"
	"test/RPCET/handler"
	"test/RPCET/config"
)

func main() {
	ctx := handler.InitContext()

	rt := handler.InitRoutes(ctx)

	serv := &http.Server{
		Handler:      rt,
		Addr:         "127.0.0.1:" + strconv.FormatUint(uint64(config.EnvConfig.ListenPort), 10),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	log.Println("starting server at:", config.EnvConfig.ListenPort)
	serv.ListenAndServe()
}
