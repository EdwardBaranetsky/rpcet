IGNORE_FAIL := || true
clean:
	docker stop testPostgres $(IGNORE_FAIL)
	docker rm testPostgres $(IGNORE_FAIL)

envrun:
	docker start testPostgres || docker run --name testPostgres -p 5432:5432 -e POSTGRES_PASSWORD=123 -e POSTGRES_DB=rpcet -d postgres

inttest:
	godep go test -tags=integration
